import React, { Component } from "react";
import classes from "./Message.css";
export default class Message extends Component {
  render() {
    return (
      <div className={classes.message} style={this.props.styles}>
        <span className={classes.message__author}>{this.props.user} : </span>{" "}
        {this.props.message}
      </div>
    );
  }
}
