import React, { Component } from "react";
import classes from "./Chatroom.css";
import Message from "../Message/Message";
import firebase from "firebase";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

class Chatroom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      msg: "",
      user: "",
      messageList: []
    };
    this.messageRef = firebase
      .database()
      .ref()
      .child("messages");
  }

  componentDidMount() {
    this.fetchMessages();
    const { user } = this.state;
    if (!user) {
      let randomUser = Math.random()
        .toString(36)
        .substring(7);
      this.setState({ user: randomUser.toString() });
    }
  }

  handleChange = e => {
    this.setState({ msg: e.target.value });
  };

  handleSubmit = () => {
    const { msg, user } = this.state;

    if (msg) {
      const newItem = {
        user: user,
        msg: msg
      };
      this.messageRef.push(newItem);
      this.setState({ msg: "" });
    }
  };

  handleKeyPress = (event) => {
    // event.preventDefault();
    if (event.key !== "Enter") return;
    else {
      event.preventDefault();
      this.handleSubmit();
    }
  }
  fetchMessages() {
    this.messageRef.on("value", snap => {
      this.setState({
        messageList: Object.values(snap.val())
      });
    });
  }

  displayMessages = () => {
    const { messageList } = this.state;
    if (messageList) {
      return messageList.map((item, index) => (
        <Message
          key={index}
          message={item.msg}
          user={item.user}
          styles={
            this.state.user === item.user
              ? { textAlign: "right" }
              : { textAlign: "left" }
          }
        />
      ));
    } else {
      return null;
    }
  };
  render() {
    return (
      <div className={classes.chatroom}>
        <div className={classes.message_list}>{this.displayMessages()}</div>
        <Form>
          <Form.Group controlId="msg">
            <Form.Control
              type="text"
              name="msg"
              value={this.state.msg}
              className={classes.inputbox}
              placeholder="Type message"
              onChange={this.handleChange}
              onKeyPress={this.handleKeyPress}
            />
            <Button variant="success" size="lg" onClick={this.handleSubmit}>
              Send
            </Button>
          </Form.Group>
        </Form>
      </div>
    );
  }
}

export default Chatroom;
