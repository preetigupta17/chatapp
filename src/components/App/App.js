import React from "react";
import classes from "./App.css";
import Chatroom from "../Chatroom";
import firebase from "firebase";
import { firebaseConfig } from "../../config";
firebase.initializeApp(firebaseConfig);

function App() {
  return (
    <div className={classes.app}>
      <Chatroom />
    </div>
  );
}

export default App;
